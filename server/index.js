const Koa = require('koa');
const cors = require('@koa/cors');
const routes = require('./routes');

const app = new Koa();

// if you want to prevent other websites from using your api, and only allow your application, limit the origin with cors
// app.use(cors({ origin: 'http://yoursite' }));

const PORT = process.env.PORT || 9999;

app.use(routes.routes());

app.use(ctx => {
  ctx.body = 'ok';
});

app.listen(PORT, () => {
  console.log(`GOKZ API listening on ${PORT}`);
});