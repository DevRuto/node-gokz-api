const Router = require('koa-router');
const db = require('../db');

const router = new Router();

router.get('/', async ctx => {
  const params = ctx.query;
  try {
    const query = db('Players');
    if (params.id) {
      query.where('SteamID32', params.id);
    }
    if (params.alias) {
      query.whereRaw('Alias LIKE ?', `%${params.alias}%`);
    }
    if (params.country) {
      query.whereRaw('Country LIKE ?', `%${params.country}%`);
    }
    query.orderBy('Alias').orderBy('Country');
    ctx.body = await query.select('*');
  } catch (err) {
    console.log(err);
    ctx.status = 500;
    ctx.body = 'Interesting. something went wrong';
  }
});

module.exports = router;