const Router = require('koa-router');
const db = require('../db');

const router = new Router();

router.get('/', async ctx => {
  const params = ctx.query;
  try {
    const query = db('Maps');
    if (params.query) {
      query.whereRaw('Name LIKE ?', `%${params.query.toLowerCase()}%`);
      query.orderBy('Name');
    } else {
      query.orderBy('MapID');
    }
    if (params.limit) {
      query.limit(params.limit);
    }
    if (params.offset) {
      query.offset(params.offset);
    }
    ctx.body = await query;
  } catch (err) {
    console.log(err);
    ctx.status = 500;
    ctx.body = 'Interesting. something went wrong';
  }
});

router.get('/:map', async ctx => {
  const map = ctx.params.map;
  if (Number.parseInt(map, 10) !== NaN) {
    ctx.body = await db('Maps').where({ MapID: map}).first()
  } else {
    ctx.body = await db('Maps').where({ Name: map }).first()
  }
});

module.exports = router;