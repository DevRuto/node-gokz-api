const Router = require('koa-router');
const db = require('../db');

const router = new Router();

router.get('/', async ctx => {
  const params = ctx.query;
  try {
    const query = db('Times as t').innerJoin('Players as p', 't.SteamID32', 'p.SteamID32');
    if (ctx.querystring) {
      query.innerJoin('MapCourses as mc', 't.MapCourseID', 'mc.MapCourseID')
                   .innerJoin('Maps as m', 'mc.MapID', 'm.MapID');
    }
    if (params.map) {
      query.whereRaw('m.Name LIKE ?', params.map.toLowerCase());
      query.orderBy('RunTime');
    } else {
      query.orderBy('t.Created', 'desc');
    }
    if (params.course) {
      query.where('mc.Course', params.course);
    }
    const times = await query.select('t.*', 'p.Alias as Player', 'm.Name as MapName', 'mc.Course as Course');
    ctx.body = times;
  } catch (err) {
    console.log(err);
    ctx.status = 500;
    ctx.body = 'Interesting. something went wrong';
  }
});

module.exports = router;