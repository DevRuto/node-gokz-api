const Router = require('koa-router');
const maps = require('./maps');
const players = require('./players');
const times = require('./times');

const router = new Router();
const BASE_URL = '/api/gokz';

router.use(`${BASE_URL}/maps`, maps.routes());
router.use(`${BASE_URL}/players`, players.routes());
router.use(`${BASE_URL}/times`, times.routes());

module.exports = router;