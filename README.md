# GOKZ API in Node

> Simple API for your GOKZ database

## Notes
* Default port: **9999** (change in `server/index.js` or `PORT` environment variable)

## Routes
> Params - all routes

* ?limit=
    * Limit the number of results returned
* ?offset=
    * Offset the position of results returned

### /api/gokz/maps
> Params

* ?query=
    * Search maps by name

### /api/gokz/maps/:term
> :term - map id or map name

* returns map object with exact map id or map name

### /api/gokz/players
> Params

* ?id=
    * Search by SteamID32
* ?alias=
    * Search by name
* ?country=
    * Search by country

### /api/gokz/times
> Params

* ?map=
    * Search for times with exact map name
* ?course=
    * Filter out by course